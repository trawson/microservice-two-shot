import React, { useEffect, useState } from 'react';


function CreateHat () {
    const[locations, setLocations] = useState([]);
    const[formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        picture_url: '',
        location: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (hatsUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                fabric: '',
                style: '',
                color: '',
                picture_url: '',
                location: '',
            });
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                </div>
                <div className="col">
                    <div className="card shadow">
                    <div className="card-body">
                        <form onSubmit={handleSubmit} id="create-hat-form">
                        <h1 className="card-title">Let's Make a Hat!</h1>
                        <p className="mb-3">
                            Choose Your Location
                        </p>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.location} name="location" id="location" className="dropdownClasses" required>
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.href} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                );
                                })}
                        </select>
                    </div>
                        <p className="mb-3">
                            Hat Info
                        </p>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} required placeholder="fabric" type="text" id="fabric" name="fabric" className="form-control"/>
                            <label htmlFor="name">Fabric</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style} required placeholder="style" type="text" id="style" name="style" className="form-control"/>
                            <label htmlFor="email">Style Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} required placeholder="color" type="text" id="color" name="color" className="form-control"/>
                            <label htmlFor="email">Color</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.picture_url} required placeholder="picture_url" type="url" id="picture_url" name="picture_url" className="form-control"/>
                            <label htmlFor="email">Picture URL</label>
                        </div>
                    </div>
                    </div>
                        <button className="btn btn-lg btn-primary">Create!</button>
                    </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    )
};

export default CreateHat;
