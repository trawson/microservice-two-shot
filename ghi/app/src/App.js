import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateHat from './CreateHat';
import DeleteHat from './DeleteHat';
import HatList from './HatsList';
import CreateShoe from './CreateShoe';
import DeleteShoe from './DeleteShoe';
import ShoeListForm from './ShoesList';

function App() {
  return(
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path="hats/new" element={<CreateHat />} />
        </Routes>
        <Routes>
          <Route path="hats/delete" element={<DeleteHat />} />
        </Routes>
        <Routes>
          <Route path="hats/" element={<HatList hats={props.hats} />} />
        </Routes>
        <Routes>
          <Route path="shoes/new" element={<CreateShoe />} />
        </Routes>
        <Routes>
          <Route path="shoes/delete" element={< DeleteShoe />} />
        </Routes>
        <Routes>
          <Route path="shoes/list" element={< ShoeListForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
