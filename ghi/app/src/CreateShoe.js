import React, { useEffect, useState } from "react";


function CreateShoe () {
    const[bins, setBins] = useState([]);
    const[formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (shoesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            });
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
            <div className="col col-sm-auto">
            </div>
            <div className="col">
            <div className="card shadow">
                <div className='card-body'>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <h1 className="card-ti tle">Lets add a pair of shoes!</h1>
                    <p className="mb-3">
                    Please choose your bin!
                    </p>
                    <div className="mb-3">
                    <select
                        onChange={handleFormChange}
                        name="bin"
                        id="bin"
                        className="dropdownClasses"
                        value={formData.bin}
                        required
                    >
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                    </select>
                    </div>
                    <p className="mb-3">Manufacturer Name</p>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input
                            required
                            onChange={handleFormChange}
                            placeholder="Manufacturer Name"
                            type="text"
                            id="manufacturer"
                            name="manufacturer"
                            className="form-control"
                            value={formData.manufacturer}
                        />
                        <label htmlFor="name">Manufacturer Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input
                            required
                            onChange={handleFormChange}
                            placeholder="Model Name"
                            type="text"
                            id="model_name"
                            name="model_name"
                            className="form-control"
                            value={formData.model_name}
                        />
                        <label htmlFor="model_name">Model Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input
                            required
                            onChange={handleFormChange}
                            placeholder="Color"
                            type="text"
                            id="color"
                            name="color"
                            className="form-control"
                            value={formData.color}
                        />
                        <label htmlFor="color">Color</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                        <input
                            required
                            onChange={handleFormChange}
                            placeholder="Picture"
                            type="url"
                            id="picture_url"
                            name="picture_url"
                            className="form-control"
                            value={formData.picture_url}
                        />
                        <label htmlFor="picture_url">Picture URL</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Add My Shoe!</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
        );
    }

export default CreateShoe;
