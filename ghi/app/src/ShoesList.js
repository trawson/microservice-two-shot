import React from 'react';



function ShoeList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map( shoe => {
                    return (
                    <tr key={shoe.href}>
                        <td>{ shoe.fabric }</td>
                        <td>{ shoe.style }</td>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.picture_url }</td>
                        <td>{ shoe.bin }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default ShoeList;
