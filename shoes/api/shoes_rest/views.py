from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.http import JsonResponse
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "bin_number",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "picture_url",
    ]
    encoders = {
        "bin":BinVOEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
    ]
    encoders = {
        "bin":BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == 'GET':
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        bin_href = content['bin']
        bin = BinVO.objects.get(import_href=bin_href)
        content['bin'] = bin
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        return JsonResponse({"error": "Method not allowed."})



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    print("what is going on")
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"error": "Shoe not found."})
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except Shoe.DoesNotExist:
            return JsonResponse({"error": "Shoe not found."})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=id)
            props = [
                "manufacturer",
                "model_name",
                "color",
                "picture_url",
                "bin"
            ]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
                shoe.save()
                return JsonResponse(
                    shoe,
                    encoder=ShoeDetailEncoder,
                    safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"error": "Shoe not found."})
