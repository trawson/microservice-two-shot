from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats, LocationVO


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href"
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "picture_url",
        "style",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        location_href = content["location"]
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        return JsonResponse({"dude. You failed. LOL"})

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request, id):
    if request.method == "GET":
        hats = Hats.objects.get(id=id)
        return JsonResponse(
            hats,
            endcoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Hats.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except Hats.DoesNotExist:
            return JsonResponse({"Abandon hope, all ye who enter here."})
    else:
        try:
            content = json.loads(request.body)
            hats = Hats.objects.get(id=id)
            props = [
                "fabric",
                "style",
                "color",
                "picture_url",
                "location",
            ]
            for item in props:
                if item in content:
                    setattr(hats, props, content[props])
            hats.save()
            Hats.objects.filter(id=id).update(**content)
            return JsonResponse(
                hats,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse({"It is hopeless. Give up. Seriously, like - just go."})
